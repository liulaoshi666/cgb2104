package com.jt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDemo1Application {

    public static void main(String[] args) {
        //整合Spring容器开始调用,加载类上的注解@SpringBootApplication
        SpringApplication.run(SpringbootDemo1Application.class, args);
    }

}
