package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.ItemDescMapper;
import com.jt.mapper.ItemMapper;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService{

    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private ItemDescMapper itemDescMapper;

    /**
     * 1.手写Sql 一种写法
     * 2.利用MP机制实现分页
     * @param pageResult
     * @return
     */
    @Override
    public PageResult getItemList(PageResult pageResult) {//3个

        IPage page = new Page(pageResult.getPageNum(),pageResult.getPageSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"title",pageResult.getQuery());
        //执行分页查询,返回值依然是分页对象信息
        page = itemMapper.selectPage(page,queryWrapper);
        //回传5个参数 total/分页后的数据
        long total = page.getTotal();
        List<Item> rows = page.getRecords();
        return pageResult.setTotal(total).setRows(rows);
    }

    @Override
    @Transactional
    public void updateItemStatus(Item item) {

        itemMapper.updateById(item);
    }

    @Override
    @Transactional
    public void deleteItemById(Integer id) {
        //删除商品表
        itemMapper.deleteById(id);
        //删除详情信息
        itemDescMapper.deleteById(id);
    }

    /**
     * 实现商品新增  Item/ItemDesc  要求ID相同 一一对应.
     * Item主键自增: 只有执行了insert 入库操作之后才会动态分配ID
     * 难点: itemDesc入库时主键为null
     * 解决方案: 实现主键自动回显功能!!!!
     * @param itemVO
     */
    @Override
    @Transactional
    public void saveItem(ItemVO itemVO) {
        Item item = itemVO.getItem();
        item.setStatus(true);   //默认启用状态
        itemMapper.insert(item);//MP自动的实现了主键自动回显
        ItemDesc itemDesc = itemVO.getItemDesc();
        itemDesc.setId(item.getId());//MP回显之后取值操作
        itemDescMapper.insert(itemDesc);
    }

}
