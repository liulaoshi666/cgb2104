package com.jt.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class SysResult {

    private Integer status; //状态码信息
    private String msg;     //提示信息
    private Object data;    //封装业务数据

    public static SysResult fail(){
        return new SysResult(201, "服务器调用失败",null );
    }

    public static SysResult success(){
        return new SysResult(200, "业务执行成功", null);
    }

    /**
     * 1.方法要被别人调用!!!
     * 2.该方法被调用时 调用者负责参数的传递.
     * @param data
     * @return
     */
    public static SysResult success(Object data){
        return new SysResult(200, "业务执行成功", data);
    }

    public static SysResult success(String msg,Object data){
        return new SysResult(200, msg, data);
    }
}

